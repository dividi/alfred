﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.IO;
using System.Diagnostics;


namespace Alfred
{
    public partial class Form1 : Form
    {

        // globals
        Dictionary<string, List<string>> groups = new Dictionary<string, List<string>>();
        List<string> tempComputerList = new List<string>();


        public Form1()
        {
            InitializeComponent();
        }



        /// <summary>
        /// Refresh / generate cache file
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void refreshToolStripMenuItem_Click(object sender, EventArgs e)
        {

            // my ldap tools
            LDAP myLDAP = new LDAP(OUField.Text, loginField.Text, passwordField.Text);


            // get the json string
            string json = myLDAP.exportToJSON();


            // get the groups
            groups = importFromJson(json);
            

            // draw the treeview
            drawTreeView(computersList, groups);
            
        }



        /// <summary>
        /// draw the treeview with groups
        /// </summary>
        /// <param name="myTV"></param>
        /// <param name="groups"></param>
        private void drawTreeView (TreeView myTV, Dictionary<string, List<string>> groups)
        {

            // clear
            myTV.Nodes.Clear();
            
            // iterate throught the groups
            foreach (KeyValuePair<string, List<string>> group in groups)
            {

                TreeNode myGroup = myTV.Nodes.Add(group.Key);

                // iterate throught the computers
                foreach (string computer in group.Value)
                {
                    myGroup.Nodes.Add(computer);
                }

            }

        }


        
        /// <summary>
        /// Add / remove a computer from LDAP list
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void computersList_AfterCheck(object sender, TreeViewEventArgs e)
        {

            // add
            if (e.Node.Checked == true)
            {

                // congrats it's a group !

                if (e.Node.Level == 0)
                {
                    e.Node.Expand();

                    foreach (TreeNode n in e.Node.Nodes)
                    {
                        n.Checked = true;
                    }
                }
                // oups a computer
                else
                {
                    try
                    {
                        // add if node don't exist
                        if (!tempComputerList.Contains(e.Node.Text))
                            tempComputerList.Add(e.Node.Text);
                    }
                    catch (Exception ex)
                    {
                        Console.WriteLine(ex);
                    }

                }

            }

            // remove
            else
            {
                // a group
                if (e.Node.Level == 0)
                {
                    foreach (TreeNode n in e.Node.Nodes)
                    {
                        n.Checked = false;
                    }

                    e.Node.Collapse();
                }
                else
                {
                    // computer
                    tempComputerList.Remove(e.Node.Text);
                }
            }


            computerPool.Text = string.Join(",", tempComputerList.ToArray());

            computersListLabel.Text = "Machines du LDAP (" + tempComputerList.Count + ")";
        }




        /// <summary>
        /// On load, draw TV, ...
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Form1_Load(object sender, EventArgs e)
        {

            // clear output file
            File.WriteAllText("output.txt", "");

            // load config
            loadConfig();


            // cache exists -> proceed
            if ( File.Exists("cache.json") )
            {

                // get the cache file
                string json = File.ReadAllText("cache.json");

                groups = importFromJson(json);

                // draw the treeview
                drawTreeView(computersList, groups);
            }  
            else
            {
                MessageBox.Show("Pas de fichier cache. Remplissez les champs de connexion et LDAP->Refresh.");

            }

            

        }






        /// <summary>
        /// Transform a json string in a complex object
        /// </summary>
        /// <param name="json"></param>
        /// <returns></returns>
        public Dictionary<string, List<string>> importFromJson(string json)
        {
            Dictionary<string, List<string>> obj = new Dictionary<string, List<string>>();
            obj = Newtonsoft.Json.JsonConvert.DeserializeObject<Dictionary<string, List<string>>>(json);
            return obj;
        }





        /// <summary>
        /// Execute a command
        /// </summary>
        /// <param name="user"></param>
        /// <param name="password"></param>
        /// <param name="pcs"></param>
        /// <param name="cmd"></param>
        /// <param name="interactive"></param>
        /// <returns>The return value must be changed</returns>
        public string[] execCMD(string user, string password, string pcs, string cmd, int timeout )
        {
            
            string[] output = new string[2];

            string currentDir = Path.GetDirectoryName(System.Reflection.Assembly.GetExecutingAssembly().Location);

            Process p = new Process();


            p.StartInfo.UseShellExecute = true;
            p.StartInfo.CreateNoWindow = false;
            p.StartInfo.WorkingDirectory = currentDir;


            if ( consoleOutput.Checked )
            {
                p.StartInfo.FileName = "cmd.exe";
                p.StartInfo.Arguments = @"/K psexec -nobanner -accepteula -s \\" + pcs + " -u " + user + " -p " + password + " -n " + timeout + " " + cmd;
            } else
            {
                p.StartInfo.FileName = "cmd.exe";
                p.StartInfo.Arguments = @"/C psexec >output.txt -nobanner -accepteula -s \\" + pcs + " -u " + user + " -p " + password + " -n " + timeout + " " + cmd;
            }

            //Console.WriteLine(p.StartInfo.Arguments);

            p.Start();
            p.WaitForExit();




            /*
            // output

            Process p = new Process();

            p.StartInfo.StandardOutputEncoding = Encoding.GetEncoding(850);
            p.StartInfo.UseShellExecute = false;

            p.EnableRaisingEvents = false;
            //p.StartInfo.CreateNoWindow = true;
            p.StartInfo.RedirectStandardOutput = false;
            p.StartInfo.RedirectStandardError = false;
            p.StartInfo.RedirectStandardInput = false;

            p.StartInfo.FileName = "psexec.exe";
            p.StartInfo.Arguments = @" -nobanner -accepteula -s \\" + pcs + " -u " + user + " -p " + password + " -n " + timeout + " '" + cmd + "' > output.txt";
            
            p.Start();
            p.WaitForExit();

            output[0] = p.StandardOutput.ReadToEnd();
            output[1] = p.StandardError.ReadToEnd();*/

            return output;


        }




        /// <summary>
        /// click run command
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void runCMD_Click(object sender, EventArgs e)
        {
            
            // nothin' in da list
            if (computerPool.Text == "")
            {
                outputBox.Text = "Pas de PC dans la liste...";
            }
            else
            {

                //outputBox.Text = Environment.NewLine + "----------------COMMAND START----------------" + Environment.NewLine;

                string[] output_arr = execCMD(loginField.Text, passwordField.Text, computerPool.Text, commandBox.Text, 5);
                
                /*
                string output = output_arr[0];
                string errors = output_arr[1];
                
                outputBox.AppendText(Environment.NewLine+ errors);

                outputBox.AppendText(Environment.NewLine + "-------------------OUTPUT-------------------" + Environment.NewLine + output);

                outputBox.AppendText(Environment.NewLine + "-----------------COMMAND END-----------------");
                */

                if (File.Exists("output.txt"))
                {
                    outputBox.Text = File.ReadAllText("output.txt", Encoding.GetEncoding(852));
                }
                

            }
        }

        


        /// <summary>
        /// load the config file
        /// </summary>
        private void loadConfig ()
        {
            if (!File.Exists("config.json")) MessageBox.Show("Pas de fichier de config trouvé...");

            try
            {
                string json = File.ReadAllText("config.json");

                var def = new { OU = "", LOGIN = @"" };

                var config = Newtonsoft.Json.JsonConvert.DeserializeAnonymousType(json, def);

                OUField.Text = config.OU;
                loginField.Text = config.LOGIN;

            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }

        }




        /// <summary>
        /// save parameters
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void sauvegarderToolStripMenuItem_Click(object sender, EventArgs e)
        {

            string json = "{'OU':'" + OUField.Text + "', 'LOGIN':'" + loginField.Text.Replace(@"\", @"\\") + "'}";

            StreamWriter sw = new StreamWriter("config.json", false, System.Text.Encoding.UTF8);

            // serialize all the groups        
            sw.WriteLine(json);

            // close the streamwriter
            sw.Close();
        }




        /// <summary>
        /// load parameters
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void chargerToolStripMenuItem_Click(object sender, EventArgs e)
        {
            loadConfig();           
        }






        /// <summary>
        /// Ping computers in the list
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void pingToolStripMenuItem_Click(object sender, EventArgs e)
        {

            outputBox.Text = "";

            List<string> list = computerPool.Text.Split( new Char [] { ',' } ).ToList();

            // nothin' in da list
            if (list.Count <1 || computerPool.Text == "") {
                outputBox.Text = "Pas de PC dans la liste...";
            }
            else
            {

                System.Net.NetworkInformation.Ping ping = new System.Net.NetworkInformation.Ping();

                int ok = 0;
                int nok = 0;

                foreach (string computer in list)
                {
                    try
                    {
                        System.Net.NetworkInformation.PingReply pingreply = ping.Send(computer.Trim());

                        if (pingreply.Status == System.Net.NetworkInformation.IPStatus.Success)
                        {
                            outputBox.AppendText(computer.Trim() + " OK\n");
                            ok++;
                        }
                        else
                        {
                            outputBox.AppendText(computer.Trim() + " NOK\n");
                        }
                    }
                    catch (Exception ex)
                    {
                        outputBox.AppendText(computer.Trim() + " NOK\n");
                        nok++;
                    } 
                }

                outputBox.AppendText("\n---------------------------------");
                outputBox.AppendText("\r\nOK: " + ok + " NOK: " + nok);
            }


        }


        
        /// <summary>
        /// Load a command file
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void openCommand_FileOk(object sender, CancelEventArgs e)
        {
            commandBox.Text = File.ReadAllText(openCommand.FileName);
        }


        private void loadCommand_Click(object sender, EventArgs e)
        {
            DialogResult filename = openCommand.ShowDialog();
        }


        // clear the command box
        private void clearCommand_Click(object sender, EventArgs e)
        {
            commandBox.Text = "";
        }
    }   // end class
}
