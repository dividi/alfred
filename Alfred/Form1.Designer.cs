﻿namespace Alfred
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form1));
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.lDAPToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.refreshToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.configToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.sauvegarderToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.chargerToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.pingToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.computersList = new System.Windows.Forms.TreeView();
            this.computerPool = new System.Windows.Forms.TextBox();
            this.runCMD = new System.Windows.Forms.Button();
            this.outputBox = new System.Windows.Forms.TextBox();
            this.commandBox = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.computersListLabel = new System.Windows.Forms.Label();
            this.passwordField = new System.Windows.Forms.TextBox();
            this.loginField = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.toolTip1 = new System.Windows.Forms.ToolTip(this.components);
            this.OUField = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.consoleOutput = new System.Windows.Forms.RadioButton();
            this.fileOutput = new System.Windows.Forms.RadioButton();
            this.openCommand = new System.Windows.Forms.OpenFileDialog();
            this.loadCommand = new System.Windows.Forms.Button();
            this.clearCommand = new System.Windows.Forms.Button();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.label6 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.menuStrip1.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.SuspendLayout();
            // 
            // menuStrip1
            // 
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.lDAPToolStripMenuItem,
            this.configToolStripMenuItem,
            this.pingToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(825, 24);
            this.menuStrip1.TabIndex = 0;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // lDAPToolStripMenuItem
            // 
            this.lDAPToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.refreshToolStripMenuItem});
            this.lDAPToolStripMenuItem.Name = "lDAPToolStripMenuItem";
            this.lDAPToolStripMenuItem.Size = new System.Drawing.Size(48, 20);
            this.lDAPToolStripMenuItem.Text = "LDAP";
            // 
            // refreshToolStripMenuItem
            // 
            this.refreshToolStripMenuItem.Name = "refreshToolStripMenuItem";
            this.refreshToolStripMenuItem.Size = new System.Drawing.Size(113, 22);
            this.refreshToolStripMenuItem.Text = "Refresh";
            this.refreshToolStripMenuItem.ToolTipText = "Lit le LDAP et génère le fichier cache";
            this.refreshToolStripMenuItem.Click += new System.EventHandler(this.refreshToolStripMenuItem_Click);
            // 
            // configToolStripMenuItem
            // 
            this.configToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.sauvegarderToolStripMenuItem,
            this.chargerToolStripMenuItem});
            this.configToolStripMenuItem.Name = "configToolStripMenuItem";
            this.configToolStripMenuItem.Size = new System.Drawing.Size(55, 20);
            this.configToolStripMenuItem.Text = "Config";
            // 
            // sauvegarderToolStripMenuItem
            // 
            this.sauvegarderToolStripMenuItem.Name = "sauvegarderToolStripMenuItem";
            this.sauvegarderToolStripMenuItem.Size = new System.Drawing.Size(152, 22);
            this.sauvegarderToolStripMenuItem.Text = "Sauvegarder";
            this.sauvegarderToolStripMenuItem.ToolTipText = "Sauvegarde la configuration";
            this.sauvegarderToolStripMenuItem.Click += new System.EventHandler(this.sauvegarderToolStripMenuItem_Click);
            // 
            // chargerToolStripMenuItem
            // 
            this.chargerToolStripMenuItem.Name = "chargerToolStripMenuItem";
            this.chargerToolStripMenuItem.Size = new System.Drawing.Size(152, 22);
            this.chargerToolStripMenuItem.Text = "Charger";
            this.chargerToolStripMenuItem.ToolTipText = "Charge la configuration si elle existe";
            this.chargerToolStripMenuItem.Click += new System.EventHandler(this.chargerToolStripMenuItem_Click);
            // 
            // pingToolStripMenuItem
            // 
            this.pingToolStripMenuItem.Name = "pingToolStripMenuItem";
            this.pingToolStripMenuItem.Size = new System.Drawing.Size(43, 20);
            this.pingToolStripMenuItem.Text = "Ping";
            this.pingToolStripMenuItem.ToolTipText = "Ping la liste des PC !";
            this.pingToolStripMenuItem.Click += new System.EventHandler(this.pingToolStripMenuItem_Click);
            // 
            // computersList
            // 
            this.computersList.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.computersList.CheckBoxes = true;
            this.computersList.Location = new System.Drawing.Point(12, 46);
            this.computersList.Name = "computersList";
            this.computersList.Size = new System.Drawing.Size(171, 307);
            this.computersList.TabIndex = 1;
            this.computersList.AfterCheck += new System.Windows.Forms.TreeViewEventHandler(this.computersList_AfterCheck);
            // 
            // computerPool
            // 
            this.computerPool.Location = new System.Drawing.Point(12, 379);
            this.computerPool.Multiline = true;
            this.computerPool.Name = "computerPool";
            this.computerPool.Size = new System.Drawing.Size(171, 185);
            this.computerPool.TabIndex = 2;
            // 
            // runCMD
            // 
            this.runCMD.Location = new System.Drawing.Point(314, 183);
            this.runCMD.Name = "runCMD";
            this.runCMD.Size = new System.Drawing.Size(75, 23);
            this.runCMD.TabIndex = 6;
            this.runCMD.Text = "Go !";
            this.toolTip1.SetToolTip(this.runCMD, "Le bouton pour envoyer le bouzin");
            this.runCMD.UseVisualStyleBackColor = true;
            this.runCMD.Click += new System.EventHandler(this.runCMD_Click);
            // 
            // outputBox
            // 
            this.outputBox.Font = new System.Drawing.Font("Lucida Console", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.outputBox.Location = new System.Drawing.Point(195, 279);
            this.outputBox.Multiline = true;
            this.outputBox.Name = "outputBox";
            this.outputBox.ScrollBars = System.Windows.Forms.ScrollBars.Both;
            this.outputBox.Size = new System.Drawing.Size(622, 285);
            this.outputBox.TabIndex = 4;
            // 
            // commandBox
            // 
            this.commandBox.Font = new System.Drawing.Font("Lucida Console", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.commandBox.Location = new System.Drawing.Point(6, 25);
            this.commandBox.Multiline = true;
            this.commandBox.Name = "commandBox";
            this.commandBox.ScrollBars = System.Windows.Forms.ScrollBars.Both;
            this.commandBox.Size = new System.Drawing.Size(302, 181);
            this.commandBox.TabIndex = 5;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(13, 360);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(66, 13);
            this.label1.TabIndex = 7;
            this.label1.Text = "Liste des PC";
            // 
            // computersListLabel
            // 
            this.computersListLabel.AutoSize = true;
            this.computersListLabel.Location = new System.Drawing.Point(12, 28);
            this.computersListLabel.Name = "computersListLabel";
            this.computersListLabel.Size = new System.Drawing.Size(109, 13);
            this.computersListLabel.TabIndex = 8;
            this.computersListLabel.Text = "Machine du LDAP (0)";
            // 
            // passwordField
            // 
            this.passwordField.Location = new System.Drawing.Point(73, 83);
            this.passwordField.Name = "passwordField";
            this.passwordField.PasswordChar = '*';
            this.passwordField.Size = new System.Drawing.Size(100, 20);
            this.passwordField.TabIndex = 11;
            this.toolTip1.SetToolTip(this.passwordField, "Le mot de passe du compte au dessus");
            // 
            // loginField
            // 
            this.loginField.Location = new System.Drawing.Point(73, 57);
            this.loginField.Name = "loginField";
            this.loginField.Size = new System.Drawing.Size(100, 20);
            this.loginField.TabIndex = 10;
            this.toolTip1.SetToolTip(this.loginField, "Le login pour lire le LDAP et executer les commandes (ne pas oublier le domaine s" +
        "i nécessaire)");
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(15, 34);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(23, 13);
            this.label3.TabIndex = 12;
            this.label3.Text = "OU";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(15, 60);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(33, 13);
            this.label4.TabIndex = 13;
            this.label4.Text = "Login";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(15, 86);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(53, 13);
            this.label5.TabIndex = 14;
            this.label5.Text = "Password";
            // 
            // OUField
            // 
            this.OUField.Location = new System.Drawing.Point(73, 31);
            this.OUField.Name = "OUField";
            this.OUField.Size = new System.Drawing.Size(100, 20);
            this.OUField.TabIndex = 9;
            this.toolTip1.SetToolTip(this.OUField, "Le chemin LDAP");
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(128)))));
            this.label2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.label2.Location = new System.Drawing.Point(168, 30);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(15, 15);
            this.label2.TabIndex = 25;
            this.label2.Text = "?";
            this.toolTip1.SetToolTip(this.label2, resources.GetString("label2.ToolTip"));
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(128)))));
            this.label7.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.label7.Location = new System.Drawing.Point(802, 263);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(15, 15);
            this.label7.TabIndex = 26;
            this.label7.Text = "?";
            this.toolTip1.SetToolTip(this.label7, "La sortie pour le mode fichier ou pour les résultats de ping.");
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(128)))));
            this.label9.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.label9.Location = new System.Drawing.Point(293, 7);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(15, 15);
            this.label9.TabIndex = 28;
            this.label9.Text = "?";
            this.toolTip1.SetToolTip(this.label9, resources.GetString("label9.ToolTip"));
            // 
            // consoleOutput
            // 
            this.consoleOutput.AutoSize = true;
            this.consoleOutput.Checked = true;
            this.consoleOutput.Location = new System.Drawing.Point(317, 102);
            this.consoleOutput.Name = "consoleOutput";
            this.consoleOutput.Size = new System.Drawing.Size(63, 17);
            this.consoleOutput.TabIndex = 18;
            this.consoleOutput.TabStop = true;
            this.consoleOutput.Text = "Console";
            this.consoleOutput.UseVisualStyleBackColor = true;
            // 
            // fileOutput
            // 
            this.fileOutput.AutoSize = true;
            this.fileOutput.Location = new System.Drawing.Point(317, 125);
            this.fileOutput.Name = "fileOutput";
            this.fileOutput.Size = new System.Drawing.Size(56, 17);
            this.fileOutput.TabIndex = 19;
            this.fileOutput.Text = "Fichier";
            this.fileOutput.UseVisualStyleBackColor = true;
            // 
            // openCommand
            // 
            this.openCommand.Filter = "TXT files|*.txt";
            this.openCommand.Title = "Coisir un fichier de commande";
            this.openCommand.FileOk += new System.ComponentModel.CancelEventHandler(this.openCommand_FileOk);
            // 
            // loadCommand
            // 
            this.loadCommand.Location = new System.Drawing.Point(317, 27);
            this.loadCommand.Name = "loadCommand";
            this.loadCommand.Size = new System.Drawing.Size(75, 23);
            this.loadCommand.TabIndex = 21;
            this.loadCommand.Text = "Charger";
            this.toolTip1.SetToolTip(this.loadCommand, "Charger un script");
            this.loadCommand.UseVisualStyleBackColor = true;
            this.loadCommand.Click += new System.EventHandler(this.loadCommand_Click);
            // 
            // clearCommand
            // 
            this.clearCommand.Location = new System.Drawing.Point(317, 56);
            this.clearCommand.Name = "clearCommand";
            this.clearCommand.Size = new System.Drawing.Size(75, 23);
            this.clearCommand.TabIndex = 22;
            this.clearCommand.Text = "Vider";
            this.toolTip1.SetToolTip(this.clearCommand, "Vide la fenetre de commande");
            this.clearCommand.UseVisualStyleBackColor = true;
            this.clearCommand.Click += new System.EventHandler(this.clearCommand_Click);
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.OUField);
            this.groupBox1.Controls.Add(this.label8);
            this.groupBox1.Controls.Add(this.passwordField);
            this.groupBox1.Controls.Add(this.loginField);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Controls.Add(this.label4);
            this.groupBox1.Controls.Add(this.label5);
            this.groupBox1.Location = new System.Drawing.Point(629, 30);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(188, 115);
            this.groupBox1.TabIndex = 23;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "config";
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.label9);
            this.groupBox2.Controls.Add(this.commandBox);
            this.groupBox2.Controls.Add(this.runCMD);
            this.groupBox2.Controls.Add(this.clearCommand);
            this.groupBox2.Controls.Add(this.consoleOutput);
            this.groupBox2.Controls.Add(this.loadCommand);
            this.groupBox2.Controls.Add(this.fileOutput);
            this.groupBox2.Location = new System.Drawing.Point(195, 30);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(405, 218);
            this.groupBox2.TabIndex = 24;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Commandes";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(198, 263);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(39, 13);
            this.label6.TabIndex = 15;
            this.label6.Text = "Output";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(128)))));
            this.label8.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.label8.Location = new System.Drawing.Point(167, 7);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(15, 15);
            this.label8.TabIndex = 27;
            this.label8.Text = "?";
            this.toolTip1.SetToolTip(this.label8, resources.GetString("label8.ToolTip"));
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(128)))));
            this.label10.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.label10.Location = new System.Drawing.Point(168, 361);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(15, 15);
            this.label10.TabIndex = 28;
            this.label10.Text = "?";
            this.toolTip1.SetToolTip(this.label10, "La liste des PC ciblés par la commande.\r\nOn peut rajouter manuellement des noms d" +
        "e machines (séparés par des virgules).\r\n\r\nLe menu Ping teste la connectivité des" +
        " machines de cette liste.");
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(825, 576);
            this.Controls.Add(this.label10);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.computersListLabel);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.outputBox);
            this.Controls.Add(this.computerPool);
            this.Controls.Add(this.computersList);
            this.Controls.Add(this.menuStrip1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MainMenuStrip = this.menuStrip1;
            this.Name = "Form1";
            this.Text = "Alfred";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem lDAPToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem refreshToolStripMenuItem;
        private System.Windows.Forms.TreeView computersList;
        private System.Windows.Forms.TextBox computerPool;
        private System.Windows.Forms.Button runCMD;
        private System.Windows.Forms.TextBox outputBox;
        private System.Windows.Forms.TextBox commandBox;
        private System.Windows.Forms.ToolStripMenuItem configToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem sauvegarderToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem chargerToolStripMenuItem;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label computersListLabel;
        private System.Windows.Forms.TextBox passwordField;
        private System.Windows.Forms.TextBox loginField;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.ToolStripMenuItem pingToolStripMenuItem;
        private System.Windows.Forms.ToolTip toolTip1;
        private System.Windows.Forms.RadioButton consoleOutput;
        private System.Windows.Forms.RadioButton fileOutput;
        private System.Windows.Forms.OpenFileDialog openCommand;
        private System.Windows.Forms.TextBox OUField;
        private System.Windows.Forms.Button loadCommand;
        private System.Windows.Forms.Button clearCommand;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label10;
    }
}

