﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.DirectoryServices;
using Newtonsoft.Json;

namespace Alfred
{
    class LDAP
    {

        private string passwd;
        private string login;
        private string path;
        public Dictionary<string, string> computers;


        // constructor
        public LDAP ( string path, string login, string passwd )
        {
            this.path = path;
            this.login = login;
            this.passwd = passwd;

            DirectoryEntry myEntry = new DirectoryEntry(path, login, passwd);

            this.computers = FindAllComputers(myEntry);            
        }


        

        /// <summary>
        /// Retrieve all computers in a specific UO
        /// </summary>
        /// <param name="entry"></param>
        /// <returns></returns>
        private Dictionary<string, string> FindAllComputers (DirectoryEntry entry)
        {
            try
            {

            
                Dictionary<string, string> computers = new Dictionary<string, string>();
            
                DirectorySearcher mySearcher = new DirectorySearcher(entry);
                mySearcher.Filter = ("(objectClass=computer)");

                foreach (SearchResult resEnt in mySearcher.FindAll())
                {
                    DirectoryEntry computer = resEnt.GetDirectoryEntry();
                    computers.Add(computer.Name.ToString().Replace("CN=", ""), computer.Parent.Name.ToString().Replace("OU=", ""));   
                }

                return computers;

            }
            catch(Exception ex)
            {
                return null;
            }
           

        }




        /// <summary>
        /// export the list o' computers to a JSON file named cache.json
        /// </summary>
        /// <returns></returns>
        public string exportToJSON()
        {
            try
            {

            
                var groups = this.computers.GroupBy(x => x.Value).ToDictionary(t=>t.Key, t=>t.Select(r=>r.Key).ToList());

                string myJson = JsonConvert.SerializeObject(groups);

                // write cache file
                if (System.IO.File.Exists("cache.json")) System.IO.File.Delete("cache.json");

                using (System.IO.StreamWriter sw = new System.IO.StreamWriter("cache.json"))
                {
                    sw.WriteLine(myJson);
                    sw.Close();
                }


                return myJson;

            }
            catch (Exception ex)
            {
                return null;
            }          

        }



    }
}
